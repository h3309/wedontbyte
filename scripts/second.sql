-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 17, 2022 at 04:06 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wedontbyte`
--

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `email_id` int(11) NOT NULL,
  `email_sender` varchar(255) NOT NULL,
  `email_receiver` int(255) NOT NULL,
  `email_subject` varchar(255) NOT NULL,
  `email_message` longtext NOT NULL,
  `email_file` varchar(255) NOT NULL,
  `email_timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `item_id` int(11) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_price` decimal(10,2) NOT NULL,
  `item_stock` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`item_id`, `item_name`, `item_price`, `item_stock`, `updated_at`) VALUES
(1, 'Cucumber', '3.00', 20, '2022-03-17 02:39:50'),
(2, 'Carrots', '4.99', 20, '2022-03-17 02:41:50'),
(3, 'Corn', '14.20', 20, '2022-03-17 02:41:56'),
(4, 'Cabbage', '6.00', 20, '2022-03-17 02:39:50'),
(5, 'Tomato', '5.50', 20, '2022-03-17 02:42:07'),
(6, 'Pineapple', '4.00', 20, '2022-03-17 02:39:50'),
(7, 'Green Grapes', '22.99', 20, '2022-03-17 02:42:16'),
(8, 'Apple', '11.99', 20, '2022-03-17 02:42:25'),
(9, 'Orange', '9.50', 20, '2022-03-17 02:42:36'),
(10, 'Watermelon', '2.99', 20, '2022-03-17 02:42:42'),
(11, 'Lamb Ribs', '28.00', 20, '2022-03-17 02:39:50'),
(12, 'Beef', '20.99', 20, '2022-03-17 02:42:57'),
(13, 'Sheep Meat', '16.50', 20, '2022-03-17 02:43:01'),
(14, 'Steak', '20.00', 20, '2022-03-17 02:39:50'),
(15, 'Chicken', '24.50', 20, '2022-03-17 02:44:08'),
(16, 'Nutella', '12.90', 20, '2022-03-17 02:44:20'),
(17, 'Sardines', '4.99', 20, '2022-03-17 02:44:18'),
(18, 'Canned Tuna', '4.00', 20, '2022-03-17 02:39:50'),
(19, 'Spicy Noodles Cup', '2.99', 20, '2022-03-17 02:44:26'),
(20, 'Frozen Peas', '4.99', 20, '2022-03-17 02:43:34'),
(21, 'Frozen Burger Meat', '9.99', 20, '2022-03-17 02:44:04'),
(22, 'Frozen French Fries', '20.50', 20, '2022-03-17 02:43:55'),
(23, 'Frozen Mixed Vegetables', '12.99', 20, '2022-03-17 02:43:40'),
(24, 'Frozen Fish', '20.00', 20, '2022-03-17 02:39:50'),
(25, 'Frozen Corns', '24.00', 20, '2022-03-17 02:39:50');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `userType` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`email_id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `email_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
