<!DOCTYPE html>
<html>
<head>
	<title>Send mail from PHP using SMTP</title>
	<link rel="stylesheet" href=" https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
</head>
<body>
<div class="container">
<h1 class="text-center">Send Email to Data Analyst</h1>
<hr>
	<?php 

		require('C:\xampp\htdocs\wedontbyte\library\php-excel-reader\excel_reader2.php');
		require('C:\xampp\htdocs\wedontbyte\library\SpreadsheetReader.php');
		require('C:\xampp\htdocs\wedontbyte\config.php');

		if(isset($_POST['sendmail'])) {
			require 'C:\xampp\htdocs\wedontbyte\PHPMailerAutoload.php';
			require 'C:\xampp\htdocs\wedontbyte\credential.php';
			// include('C:\xampp\htdocs\wedontbyte\phpmailer\class.phpmailer.php');
			// include('C:\xampp\htdocs\wedontbyte\phpmailer\class.smtp.php');

			$mail = new PHPMailer;

			// $mail->SMTPDebug = 4;                               // Enable verbose debug output

			// $mail->isSMTP();                                      // Set mailer to use SMTP
			$mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
			$mail->SMTPAuth = true;                               // Enable SMTP authentication
			$mail->Username = EMAIL_SEND;                 // SMTP username
			$mail->Password = PASS;                           // SMTP password
			$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
			$mail->Port = 587;                                    // TCP port to connect to

			$mail->setFrom(EMAIL_SEND, "We don't byte");
			$mail->addAddress($_POST['email']);     // Add a recipient

			$mail->addReplyTo(EMAIL_SEND);
			// print_r($_FILES['file']); exit;
			// for ($i=0; $i < count($_FILES['file']['tmp_name']) ; $i++) { 
				$mail->addAttachment($_FILES['file']['tmp_name'], $_FILES['file']['name']);    // Optional name
			// }
			$mail->isHTML(true);                                  // Set email format to HTML

			$mail->Subject = $_POST['subject'];
			$mail->Body    = $_POST['message'];
			// $mail->AltBody = $_POST['message'];

			if(!$mail->send()) {
			    echo 'Message could not be sent.';
			    echo 'Mailer Error: ' . $mail->ErrorInfo;
			} 

			$email_receiver1 = $_POST['email'];

			$sql = "SELECT user_id FROM user WHERE user_email = '".$email_receiver1."'";
			$result = $conn -> query($sql); 
			while($row = mysqli_fetch_assoc($result)) {
				$email_receiver = $row['user_id'];
			}
			$mimes = ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.ms-excel'];
			$email_sender = EMAIL_SEND; //session
			$email_subject = $_POST['subject'];
			$email_message = $_POST['message'];
			$email_file = '/wedontbyte/BusinessPartner/excel/'.$_FILES['file']['name'];
  			if(in_array($_FILES["file"]["type"],$mimes)){
				$uploadFilePath = 'excel/'.basename($_FILES['file']['name']);
				move_uploaded_file($_FILES['file']['tmp_name'], $uploadFilePath);
				$sqlInsert = "INSERT INTO email (email_sender, email_receiver, email_subject,email_message,email_file)
						VALUES ('$email_sender', '$email_receiver', '$email_subject', '$email_message', '$email_file')";

				if ($conn->query($sqlInsert) === TRUE) {
					echo '<script>
					alert("Successfully send the email");
					window.location.href="http://localhost/wedontbyte/BusinessPartner/bp-dashboard.php";
					</script>';
				} else {
					echo "Error: " . $sqlInsert . "<br>" . $conn->error;
				}
			  }else { 
				die("<br/>Sorry, File type is not allowed. Only Excel file."); 
			  }
		}
	 ?>
	<div class="row">
        <div class="col-md-12">
            <form role="form" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-sm-9 form-group"><br>
                        <label for="email">To Email:<span class="text-danger">*</span></label>
                        <!-- <input type="email" class="form-control" id="email" name="email" value="hasyaimanina2@gmail.com" maxlength="50" readonly> -->
						<select id="email" name="email">
							<?php 
							require('C:\xampp\htdocs\wedontbyte\config.php');
							$sql = "SELECT * FROM user WHERE userType LIKE 2";
							$result = mysqli_query($conn, $sql);
							while ($row = mysqli_fetch_array($result)):; 
							?>
							 <option value="<?php echo $row["user_email"];?>"><?php echo $row["user_email"];?></option>
							<?php endwhile; ?>
							<!-- <option value="volvo">Volvo</option> -->
						</select>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-9 form-group">
                        <label for="subject">Subject:<span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="subject" name="subject" value="Inventory Update" maxlength="50" readonly>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-9 form-group">
                        <label for="name">Message:<span class="text-danger">*</span></label>
                        <textarea class="form-control" type="textarea" id="message" name="message" placeholder="Your Message Here" maxlength="6000" rows="4" required></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-9 form-group">
                        <label for="name">File:<span class="text-danger">*</span></label>
                        <input name="file" multiple="multiple" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"  class="form-control" type="file" id="file" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-9 form-group">
                        <button type="submit" name="sendmail" class="btn btn-lg btn-success btn-block">Send</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>