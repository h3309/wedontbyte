
<html>
 <head>
 <link rel="stylesheet" href=" https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
 <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>

 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

 <script type="text/javascript" src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/jquery.tabledit.js"></script>
 
 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
 <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
 <script>
    setTimeout(function(){
        $('#alert').slideUp();
    },6000) //4000 milisecond
</script>
<?php 
include 'C:\xampp\htdocs\wedontbyte\config.php';
session_start();
$id =$_SESSION["user_id"];
if (isset($_POST['submit'])) {
    if ( isset($_POST["itemName"]) && isset($_POST["itemStock"])){
        $itemName = $_POST["itemName"];
        $itemStock =  $_POST["itemStock"];

        if(empty($itemName)){
            $errMSG ="Please Enter Item Name";
          }else if(empty($itemStock)){
            $errMSG ="Please Enter Item Stock";
          }

        if(!isset($errMSG)){
            $insert ="INSERT INTO personalinventory(userID,itemName,itemStock) VALUES ('$id','$itemName','$itemStock')";
            if (mysqli_query($conn, $insert)) {
                echo "<div id='alert' class='alert alert-success alert-dismissible fade show' role='alert'>New record has been added successfully !.<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden=true>&times;</span></button></div>";
             } else {
                echo "Error: " . $sql . ":-" . mysqli_error($conn);
             }
            
        }
    }

}

?>

<style>
    .mt-100 {
    margin-top: 100px
}

.container-fluid {
    margin-top: 50px
}

body {
    background-color: #f2f7fb
}

.card {
    border-radius: 5px;
    -webkit-box-shadow: 0 0 5px 0 rgba(43, 43, 43, 0.1), 0 11px 6px -7px rgba(43, 43, 43, 0.1);
    box-shadow: 0 0 5px 0 rgba(43, 43, 43, 0.1), 0 11px 6px -7px rgba(43, 43, 43, 0.1);
    border: none;
    margin-bottom: 30px;
    -webkit-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out
}

.card .card-header {
    background-color: transparent;
    border-bottom: none;
    padding: 20px;
    position: relative
}

.card .card-block {
    padding: 1.25rem
}

.table-responsive {
    display: inline-block;
    width: 100%;
    overflow-x: auto
}

.card .card-block table tr {
    padding-bottom: 20px
}

.table>thead>tr>th {
    border-bottom-color: #ccc
}

.table th {
    padding: 1.25rem 0.75rem
}

td,
th {
    white-space: nowrap
}

.tabledit-input:disabled {
    display: none
}

.btn-primary,
.sweet-alert button.confirm,
.wizard>.actions a {
    background-color: #4099ff;
    border-color: #4099ff;
    color: #fff;
    cursor: pointer;
    -webkit-transition: all ease-in 0.3s;
    transition: all ease-in 0.3s
}

.btn {
    border-radius: 2px;
    text-transform: capitalize;
    font-size: 15px;
    padding: 10px 19px;
    cursor: pointer
}
</style>
 </head>
 <body>
 <a href="../logout.php" class="nav-link">Log Out</a>
     <br>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                <h4 class="d-flex justify-content-center">Inventory Category</h4>
                </div>
                <div class="card-body" align="right">
                <form method="post" action="export.php">
                    <input type="submit" name="export" class="btn btn-sm btn-info" value="Generate excel file" />
                </form>
                    <!-- <a href="generate-email.php" class="btn btn-sm btn-info" role="button">Generate excel file</a> -->
                    <!-- <button type="button" class="btn btn-sm btn-primary" onclick="add()">Add new item</button>
                 -->
                 <a href="generate-email.php" class="btn btn-success">Send Email</a>
                 <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
  Add Item
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add item</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <form  method="post" enctype="multipart/form-data" action = "<?php $_PHP_SELF ?>"> 
                <div class="mb-3 row">
                    <label for="itemName" class="col col-form-label">Item Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="itemName" name="itemName">
                </div>
                </div>
                <div class="mb-3 row">
                    <label for="itemStock" class="col col-form-label">Item Stock</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="itemStock" name="itemStock">
                </div>
                </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit"name="submit" class="btn btn-primary">Save changes</button>
      </div>
    </form>

    </div>
  </div>
</div>

                <div class="card-block">
                    <div class="table-responsive">
                        <?php
                        
                        $sql = "SELECT * FROM personalinventory WHERE (userID ='$id') ";
                        $result = $conn -> query($sql);
                        // $row = mysqli_fetch_assoc($result);
                        $count = $result->num_rows;
                        $i=1;
                        ?>

                    <table class="table table-striped table-bordered" id="example-1">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Item Name</th>
                                    <th>Item Stock</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                while ($row = mysqli_fetch_assoc($result)){
                                ?>
                                <tr>
                                    <td class="tabledit-view-mode" style="cursor: pointer;"><span class="tabledit-span"><?php echo $i;?></span><input class="tabledit-input form-control input-sm" type="text" name="item_name" value="item_name" style="display: none;" disabled=""></td>
                                    <td class="tabledit-view-mode" style="cursor: pointer;"><span class="tabledit-span"><?php echo $row["itemName"] ?></span><input class="tabledit-input form-control input-sm" type="text" name="item_name" value="item_name" style="display: none;" disabled=""></td>
                                    <td class="tabledit-view-mode" style="cursor: pointer;"><span class="tabledit-span"><?php echo $row["itemStock"] ?></span><input class="tabledit-input form-control input-sm" type="text" name="item_stock" value="item_stock" style="display: none;" disabled=""></td>
                                </tr>
                                <?php
                                $i++;
                                }
                                ?>
                            </tbody>
                        </table>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 </body>
</html>

<!-- <script>
    $(document).ready(function(){
// example.php will be used to send the data to the sever database
        $('#example-1').Tabledit({
        // url: 'example.php',
        editButton: false,
        deleteButton: false,
        hideIdentifier: true,
        columns: {
        identifier: [0, 'id'],
        editable: [[2, 'first'], [3, 'last'],[3, 'nickname']]
        }
        });

}); 
function rm() {
  $(event.target).closest("tr").remove();
}

function add() {
  $("table").append("<tr>"+
  "<td>Item Name</td>"+
  "<td>Item Price</td>"+
  "<td>Item Stock</td>"+
  "</tr>");
}
</script> -->