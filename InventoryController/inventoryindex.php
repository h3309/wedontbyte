<!-- Page for inventory list in table and generate pdf button -->
<!-- UI Inventory Controller - safi 
*(generate report, alert team if restock needed, maintenance report, inventory status) 

- Access database and generate report of each BP *done*
- Update the Procurement team on Inventory restock if needed
- Have report maintenance *done*
- A dashboard that show inventory status *done*

-->

<?php

session_start();
include 'C:\xampp\htdocs\wedontbyte\config.php';

  $sql = "SELECT * FROM items";
  $result = $conn -> query($sql);

?>

<html>
 <head>
 <link rel="stylesheet" href=" https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
  <script language="JavaScript" type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.css">
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.js"></script>
 <title>Inventory List</title>
  <style>
    .mt-100 {
    margin-top: 100px
    }

    .container-fluid {
        margin-top: 50px
    }

    body {
        background-color: #f2f7fb;
        counter-reset: my-sec-counter;
    }

    #counter::before {
    /* Increment "my-sec-counter" by 1 */
    counter-increment: my-sec-counter;
    content: counter(my-sec-counter) ". ";
    }

    .card {
        border-radius: 5px;
        -webkit-box-shadow: 0 0 5px 0 rgba(43, 43, 43, 0.1), 0 11px 6px -7px rgba(43, 43, 43, 0.1);
        box-shadow: 0 0 5px 0 rgba(43, 43, 43, 0.1), 0 11px 6px -7px rgba(43, 43, 43, 0.1);
        border: none;
        margin-bottom: 30px;
        -webkit-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out
    }

    .card .card-header {
        background-color: transparent;
        border-bottom: none;
        padding: 20px;
        position: relative
    }

    .card .card-block {
        padding: 1.25rem
    }

    .table-responsive {
        display: inline-block;
        width: 100%;
        overflow-x: auto
    }

    .card .card-block table tr {
        padding-bottom: 20px
    }

    .table>thead>tr>th {
        border-bottom-color: #ccc
    }

    .table th {
        padding: 1.25rem 0.75rem
    }

    td,
    th {
        white-space: nowrap
    }
  </style>
 </head>
 <body>
 <a href="../logout.php" class="nav-link">Log Out</a>
 <script>
    $(document).ready( function () {
    $('#tableinv').DataTable();
    });
  </script>
  <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalCenterTitle">Item Details</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
 
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div><br>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h4 class="d-flex justify-content-center">Inventory Category</h4>
            <div class="d-flex justify-content-center">
              <a href="./charts.php" class="btn btn-sm btn-primary">Generate report</a>
            </div>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-striped table-bordered display" id="tableinv">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Item Name</th>
                    <th>Item Category</th>
                    <th>Item Stock</th>
                    <!-- <th>Details</th> -->
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()){  
                      // echo "<tr><td>" . $row['item_id'] . "</td><td>" . $row['item_name'] ."</td><td>". $row['item_price'] . "</td><td>". $row['item_stock'] . "</td><td><button type='button' class='btn btn-sm btn-primary' data-toggle='modal' data-target='#exampleModalCenter'>Details</button></td></tr>"; 
                      echo "<tr><td><div id='counter'></div></td><td>" . $row['item_name'] ."</td><td>". $row['item_category'] . "</td><td>". $row['item_stock'] . "</td></tr>"; 
                    }
                  }
                  else {
                    echo "0 items in list";
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
 </body>
</html>

