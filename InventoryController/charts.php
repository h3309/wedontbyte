<html>
  <head>
  <link rel="stylesheet" href=" https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
  <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
 <title>Dashboard</title>
 <style>
    .mt-100 {
    margin-top: 100px
    }

    .container-fluid {
        margin-top: 50px
    }

    body {
        background-color: #f2f7fb;
        counter-reset: my-sec-counter;
    }

    #counter::before {
    /* Increment "my-sec-counter" by 1 */
    counter-increment: my-sec-counter;
    content: counter(my-sec-counter) ". ";
    }

    .card {
        border-radius: 5px;
        -webkit-box-shadow: 0 0 5px 0 rgba(43, 43, 43, 0.1), 0 11px 6px -7px rgba(43, 43, 43, 0.1);
        box-shadow: 0 0 5px 0 rgba(43, 43, 43, 0.1), 0 11px 6px -7px rgba(43, 43, 43, 0.1);
        border: none;
        margin-bottom: 30px;
        -webkit-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out
    }

    .card .card-header {
        background-color: transparent;
        border-bottom: none;
        padding: 20px;
        position: relative
    }

    .card .card-block {
        padding: 1.25rem
    }

    .table-responsive {
        display: inline-block;
        width: 100%;
        overflow-x: auto
    }

    .card .card-block table tr {
        padding-bottom: 20px
    }

    .table>thead>tr>th {
        border-bottom-color: #ccc
    }

    .table th {
        padding: 1.25rem 0.75rem
    }

    td,
    th {
        white-space: nowrap
    }

    @media print {
      body * {
        visibility: hidden;
      }
      #section-to-print, #section-to-print * {
        visibility: visible;
      }
      #section-to-print {
        position: absolute;
        left: 0;
        top: 0;
      }
    }
  </style>
 </head>
 <body>
  <?php 

    date_default_timezone_set('Asia/Kuala_Lumpur');
    $todayDate = date('d-m-Y H:i');

    include 'C:\xampp\htdocs\wedontbyte\config.php';

    $sql = "SELECT 
        item_category as category,
        COUNT(item_category) as catTotal
      FROM items
      GROUP BY category";
    $result = $conn->query($sql);

    foreach($result as $data)
    {
      $category[] = $data['category'];
      $catTotal[] = $data['catTotal'];
    }

    $sql2 = "SELECT * FROM items";
    $result2 = $conn->query($sql2);

    $sql3 = "SELECT * FROM items WHERE item_stock < 5";
    $result3 = $conn->query($sql3);

  ?>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="input-group justify-content-center">
          <button type="button" class="btn btn-sm btn-primary" onclick="window.print();">Print/Download report</button>
        </div><br>
        <div class="card section-to-print"  id="section-to-print">
          <div class="card-header">
            <h2 class="text-center">Report Analysis</h2>
            <p class="text-center">Latest report on: <?php echo $todayDate?></p>
          </div>
          <div class="justify-content-center">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-6">
                  <h5 class="d-flex justify-content-center">Items low on stock</h5>
                  <div class="table-responsive">
                    <table class="table table-bordered display">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Item Name</th>
                          <th>Item Stock</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        if ($result3->num_rows > 0) {
                          while($row = $result3->fetch_assoc()){  
                            echo "<tr><td><div id='counter'></div></td><td>" . $row['item_name'] ."</td><td>". $row['item_stock'] . "</td></tr>"; 
                          }
                        }
                        else {
                          echo "0 items in list";
                        }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="col-md-6 border-left">
                  <h5 class="d-flex justify-content-center">Inventory Category</h5>
                  <canvas id="categoryChart"></canvas>
                </div>
              </div>
            </div>
          </div>
          <div class="card-body border-top">
          <h5 class="d-flex justify-content-center">All Inventory List</h5>
            <div class="card-block">
              <div class="table-responsive">
                <table class="table table-striped table-bordered display" id="tableinv">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Item Name</th>
                      <th>Item Category</th>
                      <th>Item Stock</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    if ($result2->num_rows > 0) {
                      while($row = $result2->fetch_assoc()){  
                        echo "<tr><td>" . $row['item_id'] . "</td><td>" . $row['item_name'] ."</td><td>". $row['item_category'] . "</td><td>". $row['item_stock'] . "</td></tr>"; 
                      }
                    }
                    else {
                      echo "0 items in list";
                    }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> 

<script>
  const labels = <?php echo json_encode($category) ?>;
  const data = {
    labels: labels,
    datasets: [{
      label: 'Amount: ',
      data: <?php echo json_encode($catTotal) ?>,
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(255, 159, 64, 0.2)',
        'rgba(255, 205, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(54, 162, 235, 0.2)',
      ],
      borderColor: [
        'rgb(255, 99, 132)',
        'rgb(255, 159, 64)',
        'rgb(255, 205, 86)',
        'rgb(75, 192, 192)',
        'rgb(54, 162, 235)',
      ],
      borderWidth: 1
    }]
  };

  const config = {
    type: 'bar',
    data: data,
    options: {
      scales: {
        y: {
          beginAtZero: true
        }
      }
    },
  };

  var myChart = new Chart(
    document.getElementById('categoryChart'),
    config
  );
</script>

 </body>
</html>