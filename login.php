<?php include 'config.php';
session_start();
if($_SERVER["REQUEST_METHOD"] == "POST"){
  if(empty(trim($_POST["email"]))){
    $email_err = "*Please enter email.";
  
  } else{
    $email = trim($_POST["email"]);
  }
  if(empty(trim($_POST["password"]))){
    $passw_err = "*Please enter correct password.";
  } else{
    $password = trim($_POST["password"]);
}

  if(empty($username_err)&&empty($passw_err)){
    $sql = "SELECT * FROM user WHERE (user_email ='$email') AND (user_password ='$password') ";
    $result = $conn -> query($sql);
    
    if($result->num_rows==1){
      $row = mysqli_fetch_assoc($result);
      session_start();
                            
      // Store data in session variables
      $_SESSION["loggedin"] = true;
      $_SESSION["user_id"] = $row["user_id"];
      echo $_SESSION["user_id"];
        if($row["userType"]==1){
          header("location: BusinessPartner/bp-dashboard.php");
        }elseif($row["userType"]==2){
          header("location: dataAnalyst/dataAnalystmain.php");
        }else{
          header("location: InventoryController/inventoryindex.php");
        }
       
    }else{
      $username_err = "*No account found with that username & password.";
        echo '
        <script type="text/javascript">
            alert("*No account found with that username"); 
            window.location.href = "login.php";
        </script>';
    }
  }
}
?>


<!DOCTYPE html>
<html>
<link
  href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.11.0/mdb.min.css"
  rel="stylesheet"
/>
<link
  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
  rel="stylesheet"
/>
<!-- Google Fonts -->
<link
  href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
  rel="stylesheet"
/>
<section class="vh-100" style="background-color: #508bfc;">
  <div class="container py-5 h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-12 col-md-8 col-lg-6 col-xl-5">
        <div class="card shadow-2-strong" style="border-radius: 1rem;">
          <div class="card-body p-5 text-center">

            <h3 class="mb-5">Sign in</h3>
            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"> 
            <div class="form-outline mb-4">
              <input type="email" id="email" class="form-control form-control-lg" name="email"/>
              <label class="form-label" for="email">Email</label>
            </div>

            <div class="form-outline mb-4">
              <input type="password" id="password" class="form-control form-control-lg" name="password"/>
              <label class="form-label" for="password">Password</label>
            </div>

            <!-- Checkbox -->
            <div class="form-check d-flex justify-content-start mb-4">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="form1Example3"
              />
              <label class="form-check-label" for="form1Example3"> Remember password </label>
            </div>

            <button class="btn btn-primary btn-lg btn-block" type="submit">Login</button>

            <hr class="my-4">
</form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

</html>
